class UserMailer < ApplicationMailer

  def account_activation(user)
    @user = user
    puts edit_account_activation_url(@user.activation_token, email: @user.email)
    mail to: user.email, subject: "Account activation"
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "Password reset"
  end
end